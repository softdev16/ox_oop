/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nitroez.ox_oop;
/**
 *
 * @author ckitt
 */
public class TestTB {
    public static void main(String[] args) {
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.setTable(0, 0);
        table.setTable(0, 2);
        table.setTable(1, 1);
        table.setTable(1, 2);
        table.setTable(2, 1);
        table.switchPlayer();
        table.setTable(0, 1);
        table.setTable(1, 0);
        table.setTable(2, 0);
        table.setTable(2, 2);
        table.checkWin();
        table.showTable();
        System.out.println(table.isFinish());
//        System.out.println(table.getWinner().getPlayer());
        if(table.getWinner()==null)System.out.println("NULL");
    }
}
