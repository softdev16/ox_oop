/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nitroez.ox_oop;

import java.util.Scanner;

/**
 *
 * @author NitroEz
 */
public class Game {
    static Scanner kb = new Scanner(System.in);
    private Player playerX, playerO;
    private Table table;

    public Game() {
        playerX = new Player('X');
        playerO = new Player('O');
        table = new Table(playerX, playerO);
    }

    public void run() {
        this.showWelcome();
        while (!table.isFinish()) {
            table.showTable();
            this.showTurn();
            this.input();
            table.checkWin();
            if (table.isFinish() || table.getRound() == 9) {
                if (table.getWinner() == null) {
                    System.out.println("Draw!!");
//                    table.setFinish();
                } else {
                    System.out.println(table.getWinner().getPlayer() + " Win");
                }
                showBye();
            }
            table.switchPlayer();
        }
    }

    public void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    public void showTurn() {
        System.out.println("Turn " + table.getTurn().getPlayer());
    }

    public void input() {
        while (true) {
            System.out.print("Please input Rol Col:");
            int row = kb.nextInt() - 1;
            int col = kb.nextInt() - 1;
            if (table.checkTable(row, col)) {
                table.setTable(row, col);
                break;
            }
            System.out.println("Error: table at row and col is not empty!!!");
        }
    }

    public void showBye() {
        System.out.println("Bye Bye.");
    }
}
